package main

import (
	"bufio"
	"fmt"
	"os"
	"test_o/than_so_hoc/phep_tinh"
)



func main() {
	// Nhập name, ngày, tháng, và năm sinh từ người dùng
    // Sử dụng bufio để nhập chuỗi có dấu
	reader := bufio.NewReader(os.Stdin)

	// Nhập tên từ người dùng
	fmt.Print("Nhập tên của bạn: ")
	name, _ := reader.ReadString('\n')

	var day, month, year int
	fmt.Print("Nhập ngày sinh: ")
	fmt.Scan(&day)

	fmt.Print("Nhập tháng sinh: ")
	fmt.Scan(&month)

	fmt.Print("Nhập năm sinh: ")
	fmt.Scan(&year)

	// Tính con số chủ đạo
	result := phep_tinh.Tinh_Tong(day, month, year)
    
	fmt.Println("Tên:", name)
	fmt.Printf("Ngày sinh: %d, Tháng sinh: %d, Năm sinh: %d\n", day, month, year)
	fmt.Printf("Kết quả thần số học: %d\n", result)
	
	switch result {
	case 1:
		fmt.Println("Bạn có tính cách sáng tạo và lãnh đạo.")
	case 2:
		fmt.Println("Tính cách của bạn thường được mô tả là nhân từ và hòa nhã.")
	case 3:
		fmt.Println("Bạn có sự nhạy bén và sự sáng tạo cao.")
	case 4:
		fmt.Println("Tính cách của bạn thường được đánh giá cao về sự ổn định và làm việc chăm chỉ.")
	case 5:
		fmt.Println("Bạn có tính cách linh hoạt và thích thử thách mới.")
	case 6:
		fmt.Println("Tính cách của bạn thường được mô tả là yên bình và thấu hiểu.")
	case 7:
		fmt.Println("Bạn có khả năng phân tích sâu sắc và tìm kiếm sự hiểu biết.")
	case 8:
		fmt.Println("Tính cách của bạn thường được đánh giá cao về sự quyết đoán và ổn định.")
	case 9:
		fmt.Println("Bạn có tính cách nhân ái và thích giúp đỡ người khác.")
	case 10:
		fmt.Println("Bạn đại diện cho sự táo bạo, đổi mới, chấp nhận rủi ro, khả năng phục hồi và đi theo tiếng nói bên trong, có tố chất lãnh đạo.")
	case 11:
		fmt.Println("Bạn đại diện cho sự tinh tế, sở hữu một trực giác vô cùng tốt, có khả năng thấu hiểu những người xung quanh.")
	case 22:
		fmt.Println("Bạn Đại diện cho sự kiên cường, thông minh và quyết đoán. Ngoài ra, đây là con số có trách nhiệm, nguyên tắc và kỷ luật trong công việc và cuộc sống.")	
    case 33: 
	    fmt.Println("Bạn Đại diện cho sự khôn ngoan, lạc quan và có lòng vị tha. Truyền cảm hứng cho người khác và mang đến một nguồn năng lượng chữa lành.")
	default:
		fmt.Println("Kết quả không phù hợp với phân loại nào đó. Hãy thử lại.")
	}
}
