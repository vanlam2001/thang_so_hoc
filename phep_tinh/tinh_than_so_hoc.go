package phep_tinh

// Tính tổng các chữ số của một số
func tinh_con_so_chu_dao(number int) int {
	sum := 0
	for number > 0 {
		sum += number % 10
		number /= 10
	}
	return sum
}

// Rút gọn một số theo yêu cầu 
func rut_gon_so(number int) int{
	if number == 11 || number == 22 || number == 33 || number == 29{
		return number
	} else if number > 9{
		return tinh_con_so_chu_dao(number)
	}
	return number
}

func Tinh_Tong(day, month, year int) int {

	daySum := rut_gon_so(tinh_con_so_chu_dao(day))
	monthSum := rut_gon_so(tinh_con_so_chu_dao(month))
	yearSum := rut_gon_so(tinh_con_so_chu_dao(year))

	result := daySum + monthSum + yearSum

	switch result {
	case 11, 22, 33:
		return result
	default:
		for result > 9 {
			result = tinh_con_so_chu_dao(result)
			if result == 11 {
				return result
			}
		}
		return result
	}
}
